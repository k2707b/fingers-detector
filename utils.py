import glob
import os
import properties
import numpy as np
import cv2
import pickle

def delete_sample_files():
    files = glob.glob(properties.capture_top_bottom_sample_path + '/*')
    for f in files:
        os.remove(f)
    files = glob.glob(properties.capture_left_right_sample_path + '/*')
    for f in files:
        os.remove(f)


def build_data_from_images(path):
    images_path = [os.path.join(r, file) for r, d, f in
                   os.walk(path) for file in f]
    images = np.array([[]])
    i = 0
    for img_path in images_path:
        # print(img_path)
        image = cv2.imread(img_path)

        if len(image.shape) == 3:
            image = image.reshape(image.shape[0]*image.shape[1]*image.shape[2])
        elif len(image.shape) == 2:
            image = image.reshape(image.shape[0]*image.shape[1])

        if i == 0:
            images = np.append(images, image)
        else:
            images = np.vstack((images, image))
        i = i+1
    return images


def build_datasets(ok_images, ko_images):
    ok_labels = np.ones((ok_images.shape[0], 1), np.float32)
    ko_labels = np.zeros((ko_images.shape[0], 1), np.float32)
    X = np.vstack((ok_images, ko_images))
    y = np.vstack((ok_labels, ko_labels))
    # X.astype(np.float32)
    return X, y


def delete_output_files():
    files = glob.glob(properties.data_path + 'output/*')
    for f in files:
        os.remove(f)

def create_directories():
    if not os.path.exists(properties.data_path + 'cache'):
        os.makedirs(properties.data_path + 'cache')
        print('Created directory {}'.format(properties.data_path + 'cache'))
    if not os.path.exists(properties.train_ok_top_bottom_images_path):
        os.makedirs(properties.train_ok_top_bottom_images_path)
        print('Created directory {}'.format(properties.train_ok_top_bottom_images_path))
    if not os.path.exists(properties.train_ko_top_bottom_images_path):
        os.makedirs(properties.train_ko_top_bottom_images_path)
        print('Created directory {}'.format(properties.train_ko_top_bottom_images_path))
    if not os.path.exists(properties.train_ok_left_right_images_path):
        os.makedirs(properties.train_ok_left_right_images_path)
        print('Created directory {}'.format(properties.train_ok_left_right_images_path))
    if not os.path.exists(properties.train_ko_left_right_images_path):
        os.makedirs(properties.train_ko_left_right_images_path)
        print('Created directory {}'.format(properties.train_ko_left_right_images_path))
    if not os.path.exists(properties.capture_left_right_sample_path):
        os.makedirs(properties.capture_left_right_sample_path)
        print('Created directory {}'.format(properties.capture_left_right_sample_path))
    if not os.path.exists(properties.capture_top_bottom_sample_path):
        os.makedirs(properties.capture_top_bottom_sample_path)
        print('Created directory {}'.format(properties.capture_top_bottom_sample_path))
    if not os.path.exists(properties.evaluation_output_folder):
        os.makedirs(properties.evaluation_output_folder)
        print('Created directory {}'.format(properties.evaluation_output_folder))





def cache_data(data, path):
    if os.path.isdir(os.path.dirname(path)):
        file = open(path, 'wb')
        pickle.dump(data, file)
        file.close()
    else:
        print('Directory doesnt exists')

def restore_data(path):
    data = dict()
    if os.path.isfile(path):
        file = open(path, 'rb')
        data = pickle.load(file)
    return data
