import numpy as np
import properties
from keras.models import model_from_json
import os
import cv2
import utils


class Predictor:

    def __init__(self):
        # delete previously evaluated images
        utils.delete_output_files()
        
    def _load_model(self, json, h5):
        model = model_from_json(open(os.path.join(properties.models_path, json)).read())
        model.load_weights(os.path.join(properties.models_path, h5))
        return model

    def predict(self, reshaped_processed_top_bottom_frame, 
                reshaped_processed_left_right_frame):

        top = np.array(reshaped_processed_top_bottom_frame, dtype=np.uint8)
        left = np.array(reshaped_processed_left_right_frame, dtype=np.uint8)

        top = top.reshape(top.shape[0], properties.capture_top_bottom_border_dim[0],
                          properties.capture_top_bottom_border_dim[1], 3)
        left = left.reshape(left.shape[0], properties.capture_left_right_border_dim[0],
                            properties.capture_left_right_border_dim[1], 3)

        top = top.astype('float32')
        top /= 255

        left = left.astype('float32')
        left /= 255

        prediction_top_bottom = self.model_top_bottom.predict_classes(top)
        prediction_left_right = self.model_left_right.predict_classes(left)

        print('Prediction top bottom frame ' + str(prediction_top_bottom))
        print('Prediction left right frame ' + str(prediction_left_right))
        return prediction_top_bottom, prediction_left_right

    def set_top_bottom_model(self, json, h5):
        self.model_top_bottom = self._load_model(json, h5)

    def set_left_right_model(self, json, h5):
        self.model_left_right = self._load_model(json, h5)
