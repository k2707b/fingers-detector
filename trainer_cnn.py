from keras import backend as K
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras.layers.core import Dropout
from keras.datasets import cifar10
from keras.utils import np_utils
from keras.optimizers import SGD, RMSprop, Adam
from keras import callbacks
import numpy as np
import matplotlib.pyplot as plt
import datetime
from sklearn.model_selection import train_test_split
import tensorflow as tf
from keras.utils import np_utils
import os
import cv2
import pickle
from keras.models import model_from_json
import properties
import utils


class TrainerCnn:

    def __init__(self):
        self.NB_EPOCH = 50
        self.BATCH_SIZE = 32
        self.VERBOSE = 1
        self.OPTIMIZER = 'rmsprop'
        self.VALIDATION_SPLIT = 0.2
        self.NB_CLASSES = 2  # number of outputs = number of images type
        self.LOSS = 'binary_crossentropy'

    def _load_data(self, path, X, y, label):
        print('Read images')
        images_path = [os.path.join(r, file) for r, d, f in
                       os.walk(path) for file in f]

        for img_path in images_path:
            img = cv2.imread(img_path)
            X.append(img)
            y.append(label)

        return X, y

    def _split_validation_with_hold_out(self, train, target, test_size):
        random_state = 25
        train, X_test, target, y_test = train_test_split(
            train, target, test_size=test_size, random_state=random_state)
        X_train, X_holdout, y_train, y_holdout = train_test_split(
            train, target, test_size=test_size, random_state=random_state)
        return X_train, X_test, X_holdout, y_train, y_test, y_holdout

    # TODO: refactor these 2 methods in one
    def _retrieve_top_bottom_data(self, X, y, use_cache):
        cache_path = os.path.join(
            properties.data_path + 'cache', 'train_top.dat')
        if use_cache:
            if not os.path.isfile(cache_path):
                X, y = self._load_data(
                    properties.data_path + 'ok_top_bottom', X, y, 1)
                X, y = self._load_data(
                    properties.data_path + 'ko_top_bottom', X, y, 0)
                utils.cache_data((X, y), cache_path)
            else:
                print('Restore train top_bottom from cache!')
                (X, y) = utils.restore_data(cache_path)
        else:
            X, y = self._load_data(
                properties.data_path + 'ok_top_bottom', X, y, 1)
            X, y = self._load_data(
                properties.data_path + 'ko_top_bottom', X, y, 0)
            utils.cache_data((X, y), cache_path)

        return X, y

    def _retrieve_left_right_data(self, X, y, use_cache):
        cache_path = os.path.join(
            properties.data_path + 'cache', 'train_left.dat')
        if use_cache:
            if not os.path.isfile(cache_path):
                X, y = self._load_data(
                    properties.data_path + 'ok_left_right', X, y, 1)
                X, y = self._load_data(
                    properties.data_path + 'ko_left_right', X, y, 0)
                utils.cache_data((X, y), cache_path)
            else:
                print('Restore train left_right from cache!')
                (X, y) = utils.restore_data(cache_path)
        else:
            X, y = self._load_data(
                properties.data_path + 'ok_left_right', X, y, 1)
            X, y = self._load_data(
                properties.data_path + 'ko_left_right', X, y, 0)
            utils.cache_data((X, y), cache_path)
        return X, y

    def _prepare_data(self, X, y, img_rows, img_cols):
        train_data = np.array(X, dtype=np.uint8)
        train_target = np.array(y, dtype=np.uint8)

        train_data = train_data.reshape(
            train_data.shape[0], img_rows, img_cols, 3)
        train_target = np_utils.to_categorical(y, self.NB_CLASSES)
        train_data = train_data.astype('float32')

        train_data /= 255

        print('Train shape:', train_data.shape)
        print('Train target shape:', train_target.shape)
        print(train_data.shape[0], 'train samples')

        X_train, X_test, X_holdout, y_train, y_test, y_holdout = self._split_validation_with_hold_out(
            train_data, train_target, 0.2)
        print('Split train: ', len(X_train))
        print('Split valid: ', len(X_test))
        print('Split holdout: ', len(X_holdout))
        return X_test, X_train, y_train, y_test, X_holdout, y_holdout

    def _train_and_save(self, X_train, y_train, X_test, y_test, input_shape, model_name):
        tbCallBack = callbacks.TensorBoard(
            log_dir='logs/fit', histogram_freq=0, write_graph=True, write_images=True)

        # architecture
        model = Sequential()
        model.add(Conv2D(32, (3, 3), input_shape=input_shape))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(32, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(64, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Flatten())
        model.add(Dense(64))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))
        model.add(Dense(2))
        model.add(Activation('sigmoid'))

        model.compile(loss=self.LOSS,
                      optimizer=self.OPTIMIZER,
                      metrics=['accuracy'])

        model.summary()

        model.fit(X_train, y_train, batch_size=self.BATCH_SIZE, epochs=self.NB_EPOCH,
                  verbose=self.VERBOSE, validation_split=self.VALIDATION_SPLIT, callbacks=[tbCallBack])
        score = model.evaluate(X_test, y_test, verbose=self.VERBOSE)
        print("Test score:", score[0])
        print('Test accuracy:', score[1])

        model_json = model.to_json()
        with open(properties.models_path + model_name + ".json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        model.save_weights(properties.models_path + model_name + ".h5")
        print("Saved model to disk")

        return model

    def _load_model(self, json, h5):
        model = model_from_json(open(os.path.join(properties.models_path, json)).read())
        model.load_weights(os.path.join(properties.models_path, h5))
        return model

    def train_and_evaluate(self):

        X_l = []
        y_l = []

        X_t = []
        y_t = []

        X_l, y_l = self._retrieve_left_right_data(X_l, y_l, False)
        X_t, y_t = self._retrieve_top_bottom_data(X_t, y_t, False)

        X_test_l, X_train_l, y_train_l, y_test_l, X_holdout_l, y_holdout_l = self._prepare_data(
            X_l, y_l, properties.capture_left_right_border_dim[0], properties.capture_left_right_border_dim[1])  # left
        X_test_t, X_train_t, y_train_t, y_test_t, X_holdout_t, y_holdout_t = self._prepare_data(
            X_t, y_t, properties.capture_top_bottom_border_dim[0], properties.capture_top_bottom_border_dim[1])  # top

        self._train_and_save(X_train_l, y_train_l, X_test_l,
                       y_test_l, (properties.capture_left_right_border_dim[0], properties.capture_left_right_border_dim[1], 3), 'model_left')
        self._train_and_save(X_train_t, y_train_t, X_test_t, y_test_t,
                       (properties.capture_top_bottom_border_dim[0], properties.capture_top_bottom_border_dim[1], 3), 'model_top')  # top


        # load the just saved models and assess them
        model = model_from_json(
            open(os.path.join(properties.models_path, 'model_top.json')).read())
        model.load_weights(os.path.join(properties.models_path, 'model_top.h5'))
        model.compile(loss=self.LOSS,
                      optimizer=self.OPTIMIZER,
                      metrics=['accuracy'])
        score = model.evaluate(X_holdout_t, y_holdout_t, verbose=self.VERBOSE)
        print("Test score top holdout:", score[0])
        print('Test accuracy top holdout:', score[1])

        model = model_from_json(
            open(os.path.join(properties.models_path, 'model_left.json')).read())
        model.load_weights(os.path.join(properties.models_path, 'model_left.h5'))
        model.compile(loss=self.LOSS,
                      optimizer=self.OPTIMIZER,
                      metrics=['accuracy'])
        score = model.evaluate(X_holdout_l, y_holdout_l, verbose=self.VERBOSE)
        print("Test score left holdout:", score[0])
        print('Test accuracy left holdout:', score[1])


